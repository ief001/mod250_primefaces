
package Interfaces;

import entities.Auction;
import entities.Product;
import java.util.List;
import javax.ejb.Local;


/**
 *
 * @author Tomba
 */
@Local
public interface AuctionFacadeLocal {

    void create(Auction projects);

    void edit(Auction projects);

    void remove(Auction projects);

    Auction find(Object id);

    List<Auction> findAll();

    List<Auction> findRange(int[] range);

    int count();

   public List<Auction> getAllActiveAuctions();
}
