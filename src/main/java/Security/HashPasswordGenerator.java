/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Security;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**
 *
 * @author alexa
 */
public class HashPasswordGenerator {
    private static String generateHash(String password) {
        String hashed = "";
        try{
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(password.getBytes(),0, password.length());
            hashed = new BigInteger(1, digest.digest()).toString(16);
        }
        catch(NoSuchAlgorithmException ex){
            System.out.println("No algorithm found for hashing");
        }
        return hashed;
    }
    
    public static void main(String [] args){
        System.out.println(generateHash("your password goes here"));
    }
    /**
     * Hash password from clear text to md5
     * @param cleartext password in clear text
     * @return password in MD5 format
     */
    public static String getHash(String cleartext){
        return generateHash(cleartext);
    }
}
