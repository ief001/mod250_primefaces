/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Auction;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
/**
 *
 * @author grp3
 */
@Stateless
public class AuctionFacade extends AbstractFacade<Auction> {

    @PersistenceContext(unitName = "justbid.com_mod250_project_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuctionFacade() {
        super(Auction.class);
    }
    
    public List<Auction> getAllActiveAuctions(){
        List<Auction> auctions = new ArrayList<>();
        List<Auction> tmp = null;
        try{
            tmp = findAll();
           // auctions = em.createNamedQuery("auction.findAllActiveAuctions", Auction.class).setParameter("currentTime", new Date(), TemporalType.TIMESTAMP).getResultList();
        }
        catch(Exception ex){
            System.out.println("Failed to fetch all active auctions");
        }
        if(tmp == null)
            return new ArrayList<>();
        Date now = new Date();
        for(Auction a : tmp){
            if(a.getEndAuction().after(now)){
                auctions.add(a);
            }
        }
        return auctions;
    }
    
}
