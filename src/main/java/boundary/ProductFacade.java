/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import entities.Product;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author grp3
 */
@Stateless
public class ProductFacade extends AbstractFacade<Product> {
    @PersistenceContext(unitName = "justbid.com_mod250_project_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public ProductFacade() {
        super(Product.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    @Override
    public void create(Product product){
        product.createAt();
        em.persist(product);
    }
    
    public List<Product> getAllProductsOrdered()
    {
     List<Product> products = null;
        try {
           products = em.createNamedQuery("products.getAllInIncreasingEndsTimeAuctionorder", Product.class).getResultList();
        } catch (NoResultException nre) 
        {
            System.out.println("Error fetching all products");
        }
        return products;
    }
    public List<Product> getAllProductsOrdered(long id)
    {
     List<Product> products = getAllProductsOrdered();
     List<Product> myProducts = new ArrayList<>();
     for(Product p : products){
         if(p.getProductPoster().getId() == id){
             myProducts.add(p);
         }
     }
     return myProducts;
    }
    
}
