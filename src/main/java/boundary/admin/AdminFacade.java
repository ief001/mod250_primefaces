/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary.admin;

import boundary.AbstractFacade;
import entities.admin.Admin;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Igor
 */
@Stateless
@RolesAllowed({"admin"})
public class AdminFacade extends AbstractFacade<Admin> {

    @PersistenceContext(unitName = "justbid.com_mod250_project_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdminFacade() {
        super(Admin.class);
    }

    public Admin findAdminByName(String uName) {
        Admin admin = null;
        try {
            admin = em.createNamedQuery("admin.control", Admin.class).setParameter("uname", uName).getSingleResult();
        } catch (NoResultException nre) {
        }
       
        return admin;
    }
    
}
