/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import structures.AuctionLength;

/**
 *
 * @author alexa
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "auction.findAllActiveAuctions", query = "SELECT a FROM Auction a where a.endAuction < :currentTime"),
    @NamedQuery(name = "auction.findAuctionByIdProduct", query = "SELECT a FROM Auction a where a.product.id = :productId"),
})
@XmlRootElement
public class Auction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(TemporalType.TIMESTAMP)
    Date startAuction;
    @Temporal(TemporalType.TIMESTAMP)
    Date endAuction;
    @OneToOne
    Bid currentBid;
    AuctionLength auctionLength;
    @Transient
    private boolean isActive; 
    @OneToOne
    @JoinColumn(name = "product_fk")
    private Product product;
    public boolean isIsActive() {
        if (endAuction !=null) {
        if (endAuction.after(new Date())) {
            return true;
        }
        }
        return isActive;
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public AuctionLength getAuctionLength() {
        return auctionLength;
    }
    public List<AuctionLength> getAllAuctionLengths(){
        return Arrays.asList(AuctionLength.values());
    }

    public void setAuctionLength(AuctionLength auctionLength) {
        this.auctionLength = auctionLength;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getStartAuction() {
        return startAuction;
    }

    public void setStartAuction(Date startAuction) {
        this.startAuction = startAuction;
    }

    public Date getEndAuction() {
        if (endAuction == null) {
            endAuction = new Date();
        }
        return endAuction;
    }
    public String getEndAuctionSt(){
        if (endAuction == null) {
            return "is not started yet";
        }
        return endAuction.toString();
    }
    public void setEndAuction(Date EndAuction) {
        this.endAuction = EndAuction;
    }

    public Bid getCurrentBid() {
        return currentBid;
    }

    public void setCurrentBid(Bid currentBid) {
        this.currentBid = currentBid;
    }

    void setAuction(Date start, Date end, Product product) {
        startAuction = start;
        endAuction = end;
        this.product = product;
        isActive = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public int convertIntoSeconds() {
        Date timeNow = new Date();
        long inSeconds = timeNow.getTime();

        if (endAuction != null) {
            long timeRemmaining = endAuction.getTime() - inSeconds;
            if (timeRemmaining > 0) {
                return Math.toIntExact(timeRemmaining);
            }
        }
        return 0;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.id);
        hash = 47 * hash + Objects.hashCode(this.startAuction);
        hash = 47 * hash + Objects.hashCode(this.endAuction);
        hash = 47 * hash + Objects.hashCode(this.currentBid);
        hash = 47 * hash + Objects.hashCode(this.auctionLength);
        hash = 47 * hash + (this.isActive ? 1 : 0);
        hash = 47 * hash + Objects.hashCode(this.product);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Auction other = (Auction) obj;
        if (this.isActive != other.isActive) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.startAuction, other.startAuction)) {
            return false;
        }
        if (!Objects.equals(this.endAuction, other.endAuction)) {
            return false;
        }
        if (!Objects.equals(this.currentBid, other.currentBid)) {
            return false;
        }
        if (this.auctionLength != other.auctionLength) {
            return false;
        }
        if (!Objects.equals(this.product, other.product)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Auction{" + "id=" + id /*+ ", startAuction=" + startAuction + ", endAuction=" + endAuction + ", currentBid=" + currentBid + ", auctionLength=" + auctionLength + ", isActive=" + isActive + ", product=" + product  */ +'}';
    }

   

    
}
