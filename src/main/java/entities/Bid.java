/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author grp3
 */
@Entity
@XmlRootElement
public class Bid implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer bidder;
    private float bidValue;
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeSt;
    @ManyToOne
    @JoinColumn(name = "product_fk")
    private Product bidedProduct;
     
    public Bid(){
        timeSt = new Date();
    }

    public Date getTimeSt() {
        return timeSt;
    }

    public void setTimeSt(Date timeSt) {
        this.timeSt = timeSt;
    }
    @XmlTransient
    public Product getBidedProduct() {
        return bidedProduct;
    }

    public void setBidedProduct(Product bidedProduct) {
        this.bidedProduct = bidedProduct;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Customer getBidder() {
        return bidder;
    }

    public void setBidder(Customer bidder) {
        this.bidder = bidder;
    }

    public float getBidValue() {
        return bidValue;
    }

    public void setBidValue(float bidValue) {
        this.bidValue = bidValue;
    }

    public Date getTimeStamp() {
        return timeSt;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeSt = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.bidder);
        hash = 79 * hash + Float.floatToIntBits(this.bidValue);
        hash = 79 * hash + Objects.hashCode(this.timeSt);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bid other = (Bid) obj;
        if (Float.floatToIntBits(this.bidValue) != Float.floatToIntBits(other.bidValue)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.bidder, other.bidder)) {
            return false;
        }
        if (!Objects.equals(this.timeSt, other.timeSt)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bid{" + "id=" + id + ", bidder=" + bidder + ", bidValue=" + bidValue + ", timeStamp=" + timeSt + '}';
    }

    
    
}
