/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlTransient;
import structures.UserInfo;

/**
 *
 * @author grp3
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Login.control", query = "SELECT l FROM Customer l WHERE l.uname = :uname")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Embedded
    private UserInfo userInfo;
    //private String IName;
    private String uname;
    private String password;
    @OneToMany(mappedBy = "bidder", cascade = CascadeType.ALL)
    private List<Bid> bidList;
    @OneToMany(mappedBy = "productPoster", cascade = CascadeType.ALL)
    private List<Product> productList;

    public Customer() {
       this.userInfo = new UserInfo();
    }
    @XmlTransient
    public List<Product> getProductList() {
        return productList;
    }
    public Product getProduct(long id){
        Product pr = null;
        for (Product p : productList) {
            if (p.getId() == id) {
                return p;
            }
        }
        return pr;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    @XmlTransient
    public List<Bid> getBidList() {
        return bidList;
    }

    public void setBidList(List<Bid> bidList) {
        this.bidList = bidList;
    }
    public List<Auction> getAuctionList(){
        List<Auction> allAuctions = new ArrayList<>();
        if (productList != null) {
            for (Product p : productList) {
                allAuctions.add(p.getAuction());
            }
        }
        return allAuctions;
    }
    
    public UserInfo getUser() {
        if (userInfo == null) {
          userInfo = new UserInfo();
        }
        return userInfo; 
    }

    public void setUser(UserInfo user) {
        this.userInfo = user;
    }
    
   /* public String getIName() {
        return IName;
    }

    public void setIName(String IName) {
        this.IName = IName;
    }*/

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = Security.HashPasswordGenerator.getHash(password);
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.id);
        hash = 53 * hash + Objects.hashCode(this.userInfo);
        hash = 53 * hash + Objects.hashCode(this.uname);
        hash = 53 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (!Objects.equals(this.uname, other.uname)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.userInfo, other.userInfo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id /*+ ", user=" + userInfo*/ + ", uname=" + uname + '}';
    }
    

    
    
}
