/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import Interfaces.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import structures.AuctionLength;
import structures.Category;

/**
 *
 * @author grp3
 */
@Entity
@Table(name = "PRODUCT")
    @NamedQueries({
    @NamedQuery(name = "products.getAllInIncreasingEndsTimeAuctionorder", query = "SELECT p FROM Product p ORDER BY p.auction.endAuction ")})
@XmlRootElement
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    Category category;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "pr_creation_date", 
        columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    Date createdDate;
    private Features features;
    private String pictureURL;
    private float price;
    @Column(name = "PR_PRODDESC")
    private String productDescription;
    private String productName;
    private boolean hasPicture;
    @JoinColumn(name = "auction_fk")
    @OneToOne(cascade = CascadeType.ALL)
    Auction auction;
    @ManyToOne
    @JoinColumn(name = "CUSTOMER_FK" )
    Customer productPoster;
    @OneToMany(mappedBy = "bidedProduct",cascade = CascadeType.ALL)
    private List<Bid> allBids;
    
    public Product() {
        auction = new Auction();
    }
    @XmlTransient
    public Auction getAuction() {
        if (auction == null) {
            auction = new Auction();
        }
        return auction;
    }
    
    public void setAuction(Auction auction) {
        if (auction == null) {
            auction = new Auction();
        }
        this.auction = auction;
    }
    public void setAuction( Date start, Date end){
        if (auction == null) {
         auction = new Auction();
        }
        auction.setAuction(start,end, this);
    }

    public Date getCreatedDate() {
        return new Date();
    }
    public void setCreatedDate(Date date){
        this.createdDate = date;
    }
    @PrePersist
    public void createAt(){
        this.createdDate = new Date();
        
    }
    @XmlTransient
    public List<Bid> getAllBids() {
        if(allBids != null)
            return allBids;
        return new ArrayList<Bid>();
    }

    public void setAllBids(List<Bid> allBids) {
        this.allBids = allBids;
    }
    public void setNewBid(Bid bid, Customer customer){
        if (auction == null) {
            auction = new Auction();
        }
        bid.setBidedProduct(this);
        bid.setBidder(customer);
        auction.setCurrentBid(bid);
        this.price = bid.getBidValue();
        allBids.add(bid);
    }

    public float getPrice() {
        getHighestBid();
        return price;
    }
    public void getHighestBid() {

        if (allBids != null && !allBids.isEmpty()) {
            for (Bid bid : allBids) {
                float value = bid.getBidValue();
                if (price < value) {
                    price = value;
                }
            }

        }

    }

    public void setPrice(float price) {
        this.price = price;
    }
    @XmlTransient
    public Customer getProductPoster() {
        return productPoster;
    }

    public void setProductPoster(Customer productPoster) {
        this.productPoster = productPoster;
    }
    
    public List<Category> getAllCategories(){
        return Arrays.asList(Category.values());
    }
    

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    public String getProductName(){
        return this.productName;
    }
    public void setProductName(String name){
        this.productName = name;
    }
    public String getPictureURL(){
        if (pictureURL == null) {
            return "noimage.jpg";
        }
        return this.pictureURL;
    }
    public void setPictureUrl(String picture){
        
        this.pictureURL = picture;
    }

    public boolean isHasPicture() {
        return hasPicture;
    }

    public void setHasPicture(boolean hasPicture) {
        this.hasPicture = hasPicture;
    }
  
    public String getProductDescription(){
        return this.productDescription;
    }
    public void setProductDescription(String description){
        this.productDescription = description;
    }
    public Features getFeatures(){
        return this.features;
    }
    public void setFeature(Features features){
        this.features = features;
    }
    public List<AuctionLength> getAllAuctionLengths(){
        return Arrays.asList(AuctionLength.values());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + Objects.hashCode(this.id);
        hash = 23 * hash + Objects.hashCode(this.productName);
        hash = 23 * hash + Objects.hashCode(this.pictureURL);
        hash = 23 * hash + Objects.hashCode(this.productDescription);
        hash = 23 * hash + Objects.hashCode(this.features);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.productName, other.productName)) {
            return false;
        }
        if (!Objects.equals(this.pictureURL, other.pictureURL)) {
            return false;
        }
        if (!Objects.equals(this.productDescription, other.productDescription)) {
            return false;
        }
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", owner=" + productPoster + ", auction=" + auction + ", productDescription=" + productDescription + /*", features=" + features */ '}';
    }
}
