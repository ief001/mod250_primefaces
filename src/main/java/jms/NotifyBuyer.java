/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jms;

import boundary.AuctionFacade;
import entities.Auction;
import entities.Product;
import java.util.Map;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSDestinationDefinition;
import javax.jms.Queue;
import javax.jms.Topic;


/**
 *
 * @author hestelandsbyen
 */
@JMSDestinationDefinition(
        name = "java:comp/jms/NotifyBuyerTopic",
        interfaceName = "javax.jms.Topic",
        destinationName = "PhysicalBuyerNotificationTopic")
@Named
@RequestScoped
public class NotifyBuyer {

    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;
    
    @Resource(lookup = "java:comp/jms/NotifyBuyerTopic")
    private Topic topic;
    @EJB
    private AuctionFacade af;
    private String message;
    /**
     * Posts a TextMessage to the topic NotifyBuyerTipic Topic, consisting of
     * the parameters, tab-separated
     *
     * @param p
     * 
     */
    /*    public void postMessage(String teacher, String student, String project, String url) {

     String message = teacher + "\n" + student + "\n" + project + "\n" + url;
     context.createProducer().send(topic, message);
     }*/
    public void sendMessageTowinner(Product p) {
        String uname=p.getAuction().getCurrentBid().getBidder().getUname();
        String prodName = p.getProductName() + " : "+p.getProductDescription();
        try {
            
            message = String.format( "---- START EMAIL to customer %s ----\n" +
                    "Dear%s,\n" +
                    "Congratulations! You have won in bidding for product %s.\n" +
                    "You can access the product using the following link:\n" +
                    "%s\n" +
                    "---- END EMAIL to customer %s ----",  uname, uname, prodName, "here will be product link", uname);
            context.createProducer().send(topic, message);
            
        } catch (Exception ex) {
            System.err.println("something goes worng");
        }

    }
    public String notifyTest(){
        FacesContext fc = FacesContext.getCurrentInstance();
        
	  long auctionId = getAuctionId(fc);
        Auction auction = af.find(auctionId);
        sendMessageTowinner(auction.getProduct());
          return "index";
    }
    public long getAuctionId(FacesContext fc){

		Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
		long id = 3/*Long.getLong(params.get("id"))*/;
                System.out.println(params);
                System.out.println(id);
        return id;
	}
}
