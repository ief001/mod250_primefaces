/*
 * To chang, this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.ProductFacade;
import entities.Bid;
import entities.Product;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.StringEscapeUtils;
import org.primefaces.push.EventBus;
import org.primefaces.push.EventBusFactory;

/**
 *
 * @author Igor
 */
@ManagedBean
@SessionScoped
public class AuctionView implements Serializable{
    private final static String CHANNEL = "/notify";
    private Product product;
    private CustomerView custW;
    private int convertIntoSeconds;
    @EJB
    ProductFacade pf;
    private Bid bid;
    private EventBus eventBus;
    
    /**
     * Creates a new instance of Auction
     */
    public AuctionView() {
        bid = new Bid();
        eventBus = EventBusFactory.getDefault().eventBus();
    }

    public CustomerView getCustW() {
        return custW;
    }

    public void setCustW(CustomerView custW) {
        this.custW = custW;
    }


    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = pf.find(product.getId());
    }

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }
    
   
    public String giveBid(){
        if (custW.isIsLoggedIn()) {
             this.product = pf.find(product.getId());
            if(bid.getBidValue() > product.getPrice()+1){
                float actprice =bid.getBidValue()- product.getPrice();
                product.setNewBid(bid, custW.getCustomer());
                pf.edit(product);
                
                eventBus.publish(CHANNEL, 
                        new FacesMessage("user: "+bid.getBidder().getUname()+" bided For product "+product.getProductName()+
                                " \n", "New price is with: "+
                                actprice+" higher than before"));
                bid = new Bid();
                return "/product/view";
            }
             FacesContext context = FacesContext.getCurrentInstance();
         
        context.addMessage(null, new FacesMessage("Warning",  "Your bid is to low ") );
        context.addMessage(null, new FacesMessage("Tips", "give value higher than"+product.getPrice()+1+"for valid bid"));
//            FacesMessage facesMsg = new FacesMessage(FacesMessage.SEVERITY_WARN, "your bid is too low please give a higher bid", "your bid is too low please give a higher bid");
//            FacesContext.getCurrentInstance().addMessage("Warning", facesMsg);
        bid = new Bid();
        return "/product/view";
        }
     FacesMessage facesMsg = new FacesMessage("you must be logged to bid");
     FacesContext.getCurrentInstance().addMessage("warning", facesMsg);
    return "/users/index";
    }
    public void startAuction(){
        Date startAuction = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(startAuction);
        cal.add(Calendar.MINUTE, 10);
        Date endAuction = cal.getTime();
        product.setAuction(startAuction, endAuction);
        pf.edit(product);
        
    }
    
    public void auctionEnded(){
        
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "bid ended", null));
    }
    public int convertIntoSeconds(){
        Date timeNow = new Date();
        long inSeconds = timeNow.getTime();
        
        if (product != null) {
            if (product.getAuction() != null) {
                if (product.getAuction().getAuctionLength() != null) {
                    long timeRemmaining = product.getAuction().getEndAuction().getTime() - inSeconds;
                    if (timeRemmaining > 0) {
                        return Math.toIntExact(timeRemmaining);
                    }
                }

            }

        }
        
        return 10;
    }
    public String productDetail(){
        if (product != null) {
            return "/product/productdetail.xhtml?faces-redirect=true&productId="+product.getId();
        }
        return "/product/productdetail.xhtml?faces-redirect=true&productId="+2;
        
    }

    public int getConvertIntoSeconds() {
        return convertIntoSeconds();
    }

    public void setConvertIntoSeconds(int convertIntoSeconds) {
        this.convertIntoSeconds = 0;
    }
        
}