/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author ander
 */
@Named(value = "chartView")
@Dependent
@ManagedBean
@SessionScoped
public class ChartView implements Serializable {

    private LineChartModel lineModel1;

    public LineChartModel getLineModel1() {
        return lineModel1;
    }

    public ChartView() {
        lineModel1 = new LineChartModel();

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Series 1");
        series1.set(1, 2);
        series1.set(2, 5);
        series1.set(3, 2);
        series1.set(4, 1);
        series1.set(5, 2);
        series1.set(6, 4);
        
        lineModel1.addSeries(series1);
        lineModel1.setTitle("Test");
        lineModel1.setLegendPosition("e");
        
        Axis yAxis = lineModel1.getAxis(AxisType.Y);
        yAxis.setMax(10);
        yAxis.setMin(0);
    }
    

//    private LineChartModel lineModel1;
//     
//    @PostConstruct
//    public void init() {
//        createLineModels();
//    }
// 
//    public LineChartModel getLineModel1() {
//        return lineModel1;
//    }
//     
//    private void createLineModels() {
//        lineModel1 = initLinearModel();
//        lineModel1.setTitle("Linear Chart");
//        lineModel1.setLegendPosition("e");
//        Axis yAxis = lineModel1.getAxis(AxisType.Y);
//        yAxis.setMin(0);
//        yAxis.setMax(10);
//    }
//     
//    private LineChartModel initLinearModel() {
//        LineChartModel model = new LineChartModel();
// 
//        LineChartSeries series1 = new LineChartSeries();
//        series1.setLabel("Series 1");
// 
//        series1.set(1, 2);
//        series1.set(2, 1);
//        series1.set(3, 3);
//        series1.set(4, 6);
//        series1.set(5, 8);
// 
//        model.addSeries(series1);
//         
//        return model;
//    }    
}
