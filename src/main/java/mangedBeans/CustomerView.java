/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.CustomerFacade;
import boundary.GroupsFacade;
import boundary.ProductFacade;
import entities.Customer;
import entities.Groups;
import entities.Product;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author grp3
 */
@ManagedBean
@SessionScoped
@RolesAllowed({"admin", "customer"})
public class CustomerView implements Serializable{
    
    private Customer customer;
    private Product postedProduct;
    private boolean isLoggedIn;
    private String dialogPage;
    
    @EJB
    CustomerFacade cf;
    @EJB
    ProductFacade pf;
    @EJB
    GroupsFacade gf;
    public CustomerView() 
    {
        isLoggedIn = false;
    }

    
    public Product getPostedProduct() {
        if (postedProduct == null) {
            postedProduct = new Product();
        }
        return postedProduct;
    }

    public void setPostedProduct(Product postedProduct) {
            this.postedProduct = postedProduct;    
    }
    
    public boolean isIsLoggedIn() {
        return isLoggedIn;
    }
    public void loggInByName(String uName){
        customer = this.cf.findCustomerByUName(uName);
        if(customer != null)
            isLoggedIn = true;
    }

    public String logOut(){
        isLoggedIn = false;
        customer = null;
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            HttpSession session = request.getSession();
            session.invalidate();
        }
        catch(Exception e) {
            System.out.println("Error during logout");
            //destination = "/loginerror?faces-redirect=true";
        }
        return "/faces/index.xhtml";
    }
     
    public Customer getCustomer() {
        if (customer == null) {
            customer = new Customer();
        }
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
    
    public void registerUser()
    {
        this.cf.create(customer);
        Groups group = new Groups();
        group.setGroupid("customer");
        group.setUsername(customer.getUname());
        this.gf.create(group);
        FacesMessage message = new FacesMessage("Succesful. user " + customer.getUname()+ " is registered.");
        FacesContext.getCurrentInstance().addMessage("growl", message);
        customer = new Customer();

    }
    public String postProduct()
    {
        postedProduct.setProductPoster(customer);
        pf.create(postedProduct);
        FacesMessage message = new FacesMessage("Succesful. Product " + postedProduct.getProductName() + " is registered.");
        FacesContext.getCurrentInstance().addMessage("growl", message);
        postedProduct = new Product();
        return "/product/view";
    }
    public String seePostedProducts(){
        return "/customer/manageproducts";
    }

    public String getDialogPage() {
        if (dialogPage == null) {
            return "view";
        }
        return dialogPage;
    }

    public void setDialogPage(String page)
    {
        
        this.dialogPage = page;
    }
    public void startAuction(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Date startAuction = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(startAuction);
        cal.add(Calendar.MINUTE, 20160 );
        Date endAuction = cal.getTime();
        customer.getProduct(getProdId(fc)).setAuction(startAuction, endAuction);
        cf.edit(customer); 
    }

    private long getProdId(FacesContext fc) {
        Map<String, String> prodId = fc.getExternalContext().getRequestParameterMap();
        return Long.valueOf(prodId.get("prodId"));
    }
    
    /**
     * Upload a file from jsf
     * @param event 
     */
    public void fileUpload(FileUploadEvent event){
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String img = saveFile(event.getFile());
        postedProduct.setPictureUrl(img);
        System.out.println("setting picture url");
        postedProduct.setHasPicture(true);
        System.out.println("p: = " + postedProduct.isHasPicture());
    }
    /**
     * Save an uploaded picture to the resources folder in the project.
     * @param event
     * @return 
     */
    private String saveFile(UploadedFile image){
        String uploaded = "";
        try{
            String filename = image.getFileName();
            try (InputStream input = image.getInputstream()) {
                ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                        .getExternalContext().getContext();
                String realPath = ctx.getRealPath("/");
                String imgFolderPath = realPath.substring(0, realPath.length()-35) + "src" + File.separatorChar +  "main" + File.separatorChar + "webapp" + File.separatorChar +
                        "resources" + File.separatorChar + "images" + File.separatorChar + "productimg" + File.separatorChar;
                File file = new File(imgFolderPath, filename);
                int i = 0;
                while(file.exists()){
                    int ind = filename.lastIndexOf('.');
                    String name = filename.substring(0,ind);
                    String ext = filename.substring(ind, filename.length());
                    file = new File(imgFolderPath, name + "(" + i++ + ")" + ext);
                }
                uploaded = file.getName();
                try (OutputStream output = new FileOutputStream(file)) {
                    int read;
                    byte [] bytes = new byte[1024];
                    while((read = input.read(bytes)) != -1){
                        output.write(bytes, 0 , read);
                    }
                }
            }
        }
        catch(Exception e){
            System.out.println("Problemer saving file: " + e.getMessage());
        }
        return uploaded;
    }
    
    public String goToMyProfile(){
        return "/cutomer/myprofile";
    }
}