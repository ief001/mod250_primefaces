/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.CustomerFacade;
import entities.Customer;
import java.security.Principal;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import mangedBeans.admin.AdminView;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;


/**
 *
 * @author Igor
 */
@ManagedBean
@RequestScoped
@RolesAllowed({"admin", "customer"})
public class LoginController {
    @ManagedProperty(value = "#{customerView}")
    private CustomerView customerView;
    @ManagedProperty(value = "#{adminView}")
    private AdminView adminView;
    @EJB
    private CustomerFacade cf;
    private Customer user;
    private String userName;
    private String password;

    public LoginController() 
    {
    }
    private void loginUser(){
        customerView.loggInByName(userName);
    }
    private void loginAdmin(){
        adminView.loggInAdminByName(userName);
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public AdminView getAdminView() {
        return adminView;
    }

    public void setAdminView(AdminView adminView) {
        this.adminView = adminView;
    }

    public CustomerView getCustomerView() {
        return customerView;
    }

    public void setCustomerView(CustomerView customerView) {
        this.customerView = customerView;
    }
    @RolesAllowed({"admin", "customer"})
    public void setUserLoggedInUsername() {
        if (user == null) {
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null) {
                user = cf.findCustomerByUName(principal.getName()); // Find User by j_username.
            }
        }
        if (user != null) {
            userName = user.getUname();
            loginUser();
        }
        
    }
    @RolesAllowed({"admin"})
    public void setAdminLoggedInUsername(){
        if (user == null) {
            Principal principal = FacesContext.getCurrentInstance().getExternalContext().getUserPrincipal();
            if (principal != null) {
                user = cf.findCustomerByUName(principal.getName()); // Find User by j_username.
            }
        }
        userName = user.getUname();
        loginAdmin();
    }
}
