/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans;

import boundary.ProductFacade;
import entities.Customer;
import entities.Product;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
/**
 *
 * @author Igor
 */

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
@Named(value = "productView")
@RequestScoped
public class ProductView implements Serializable{
    @ManagedProperty(value = "#{auction}")
    private AuctionView auction;
    List<Product> allProducts;
    Product  product;
    @EJB
    ProductFacade productFacade;
    
    public ProductView() {
        this.product = new Product();
        allProducts = new ArrayList<>();
    }

    public AuctionView getAuction() {
        return auction;
    }

    public void setAuction(AuctionView auction) {
        this.auction = auction;
    }
    
    
    public String findProductByIndex(Long id){
        this.productFacade.find(id);
       return "index";
    }
    
    public List<Product> getAllProducts(){
        return this.productFacade.findAll();
    }
    public List<Product> getActiveProducts(){
        List<Product> activeProd= new ArrayList<>();
        for (Product p : getAllProducts()) {
            if (p.getAuction().isIsActive()) {
                activeProd.add(p);
            }
        }
        return activeProd;
    }
    
    public List<Product> getOrderedProductsByDate(){
        List<Product> tmp = getActiveProducts();
        Collections.sort(tmp, new Comparator<Product> () {
            @Override
            public int compare(Product o1, Product o2) {
                if(o1.getAuction().getEndAuction() != null && o2.getAuction().getEndAuction() != null)
                    return (o1.getAuction().getEndAuction().compareTo(o2.getAuction().getEndAuction()));
                return 0;
            }
        });
        return tmp;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
//    public String productDetail(){
//        return "/faces/product/productdetail.xhtml";
//    }
}

