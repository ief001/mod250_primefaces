/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mangedBeans.admin;

import boundary.CustomerFacade;
import boundary.admin.AdminFacade;
import entities.Customer;
import entities.admin.Admin;
import java.io.Serializable;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Igor
 */
@ManagedBean
@SessionScoped
@RolesAllowed({"admin"})
public class AdminView implements Serializable{
    private Admin admin;
    private Customer customer;
    private List<Customer> allCustomers;
    @EJB
    AdminFacade af;
    @EJB
    CustomerFacade cf;
    private boolean isLogedIn;
    String userName;
    String password;

    public boolean isIsLogedIn() {
        return isLogedIn;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<Customer> getAllCustomers() {
        allCustomers = cf.findAll();
        return allCustomers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    public AdminView() {
        admin = new Admin();
    }
    public void loggInAdminByName(String uName){
        admin = this.af.findAdminByName(uName);
        if(admin != null)
            isLogedIn = true;
    }
    public boolean loggIn(String uName, String pass) {
        isLogedIn =false;
        admin = this.af.findAdminByName(uName);
        if (admin != null && admin.getAdminName().equals(uName) && admin.getPassword()
                                            .equals(pass)) {
            isLogedIn = true;
            return isLogedIn;
        }
        return isLogedIn;
    }
    public String logOut(){
        isLogedIn = false;
        admin = null;
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
            HttpSession session = request.getSession();
            session.invalidate();
        }
        catch(Exception e) {
            System.out.println("Error during logout");
            //destination = "/loginerror?faces-redirect=true";
    }
        return "/index";
    }

    public Admin getAdmin() {
        return admin;
    }

    public void setAdmin(Admin admin) {
        this.admin = admin;
    }
    
    
}
