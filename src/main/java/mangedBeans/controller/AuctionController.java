package mangedBeans.controller;

import entities.Auction;
import Interfaces.util.JsfUtil;
import Interfaces.util.JsfUtil.PersistAction;
import boundary.AuctionFacade;
import boundary.CustomerFacade;
import entities.Customer;
import entities.Product;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("auctionController")
@SessionScoped
//@DeclareRoles({"admin","customer"})
public class AuctionController implements Serializable {

    @EJB
    private AuctionFacade af;
    @EJB
    private CustomerFacade cf;
    private List<Auction> items = null;
    private List<Auction> customerAuctions = null;
    private Auction selected;
    private long custId;
    private long prodId;

    public AuctionController() {
    }

    public long getProdId() {
        
        return prodId;
    }

    public void setProdId(long prodId) {
        this.prodId = prodId;
    }

    public long getCustId() {
        return custId;
    }

    public void setCustId(long custId) {
        this.custId = custId;
    }

    public List<Auction> getCustomerAuctions() {
        return customerAuctions;
    }

    public void setCustomerAuctions(List<Auction> customerAuctions) {
        this.customerAuctions = customerAuctions;
    }

    public Auction getSelected() {
        return selected;
    }

    public void setSelected(Auction selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private AuctionFacade getFacade() {
        return af;
    }

    public Auction prepareCreate() {
        selected = new Auction();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("AuctionCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("AuctionUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("AuctionDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Auction> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }
    public String getCustomersAuctionItems(javax.faces.event.ActionEvent event){
        long id = (long) event.getComponent().getAttributes().get("customerId");
        customerAuctions = new ArrayList<>();
       List<Product> productList= cf.find(id).getProductList();
        if (productList !=null) {
            for (Product p : productList) {
                customerAuctions.add(p.getAuction());
            }
        }
        return "/crud/auction/List";
    }
    public List<Auction> getAuctionItemsFromCust(){
         Customer customer = cf.find(custId);
        
        if (customer !=null) {
            return customer.getAuctionList();
        }
        return new ArrayList<>();
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Auction getAuction(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<Auction> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Auction> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Auction.class)
    public static class AuctionControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            AuctionController controller = (AuctionController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "auctionController");
            return controller.getAuction(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Auction) {
                Auction o = (Auction) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Auction.class.getName()});
                return null;
            }
        }

    }

}
