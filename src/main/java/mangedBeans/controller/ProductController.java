package mangedBeans.controller;

import entities.Product;
import Interfaces.util.JsfUtil;
import Interfaces.util.JsfUtil.PersistAction;
import boundary.AuctionFacade;
import boundary.ProductFacade;
import entities.Auction;
import entities.Customer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;

@Named("productController")
@SessionScoped
public class ProductController implements Serializable {

    @EJB
    private ProductFacade ejbFacade;
    @EJB
    AuctionFacade af;
    private List<Product> items = null;
    private Product selected;
    private long prodId;
    private Customer owner;
    private boolean skip;
    public ProductController() {
    }

    public long getProdId() {
        return prodId;
    }

    public Customer getOwner() {
        return owner;
    }

    public void setOwner(Customer owner) {
        this.owner = owner;
    }

    public void setProdId(long prodId) {
        this.prodId = prodId;
    }
    public Product getPrFromId(){
        
     return getProduct(prodId);
    
    }
 
    public Product getSelected() {
        return selected;
    }

    public void setSelected(Product selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ProductFacade getFacade() {
        return ejbFacade;
    }

    public Product prepareCreate() {
        selected = new Product();
        
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        selected.setAuction(new Auction());
        selected.setProductPoster(owner);
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ProductCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ProductUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ProductDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            owner.getProductList().remove(selected);
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<Product> getItems() {
        items = getFacade().getAllProductsOrdered(owner.getId());
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                    items.add(selected);
                    owner.setProductList(items);
                } else {
                    getFacade().remove(selected);
                    af.remove(selected.getAuction());
                    
                }
                JsfUtil.addSuccessMessage(successMessage);
            }
            catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public Product getProduct(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<Product> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<Product> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = Product.class)
    public static class ProductControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProductController controller = (ProductController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "productController");
            return controller.getProduct(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Product) {
                Product o = (Product) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), Product.class.getName()});
                return null;
            }
        }

    }
    public String seePostedProducts(){
        return "/product/crud/List";
    }
     public boolean isSkip() {
        return skip;
    }
 
    public void setSkip(boolean skip) {
        this.skip = skip;
    }
     
    public String onFlowProcess(FlowEvent event) {
        if(skip) {
            skip = false;   //reset in case user goes back
            return "confirm";
        }
        else {
            return event.getNewStep();
        }
    }
        /**
     * Upload a file from jsf
     * @param event 
     */
    public void fileUpload(FileUploadEvent event){
        FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
        FacesContext.getCurrentInstance().addMessage(null, message);
        String img = saveFile(event.getFile());
        selected.setPictureUrl(img);
        selected.setHasPicture(true);
        System.out.println("uploaded: " + img);
           
    }
        /**
     * Save an uploaded picture to the resources folder in the project.
     * @param event
     * @return 
     */
    private String saveFile(UploadedFile image){
        String uploaded = "";
        try{
            String filename = image.getFileName();
            try (InputStream input = image.getInputstream()) {
                ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance()
                        .getExternalContext().getContext();
                String realPath = ctx.getRealPath("/");
                String imgFolderPath = realPath.substring(0, realPath.length()-35) + "src" + File.separatorChar +  "main" + File.separatorChar + "webapp" + File.separatorChar +
                        "resources" + File.separatorChar + "images" + File.separatorChar + "productimg" + File.separatorChar;
                File file = new File(imgFolderPath, filename);
                int i = 0;
                while(file.exists()){
                    int ind = filename.lastIndexOf('.');
                    String name = filename.substring(0,ind);
                    String ext = filename.substring(ind, filename.length());
                    file = new File(imgFolderPath, name + "(" + i++ + ")" + ext);
                }
                uploaded = file.getName();
                try (OutputStream output = new FileOutputStream(file)) {
                    int read;
                    byte [] bytes = new byte[1024];
                    while((read = input.read(bytes)) != -1){
                        output.write(bytes, 0 , read);
                    }
                }
            }
        }
        catch(Exception e){
            System.out.println("Problemer saving file: " + e.getMessage());
        }
        return uploaded;
    }
}


