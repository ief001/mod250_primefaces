/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package structures;

import entities.Customer;
import entities.Product;

/**
 *
 * @author grp3
 */
public class Message {
    private String msg;
    
    
    public Message(Customer customer, Product product, Status stat){
      if(stat == Status.SUCCESS){
          msg = String.format("Customer %s's bid has been successfully placed for product %s\n", customer.getUname(), product.getProductName());
      }
      else{
          msg = String.format("The bid for product %s has not been plced for customer %s. Status code: %s\n",
                  product.getProductName(), customer.getUname(), stat);
          
      }
    }
    
    @Override
    public String toString(){
        return this.msg;
    }
}
