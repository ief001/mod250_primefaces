/*
 * Enum for checking if a message is a success or fail
 */
package structures;

/**
 *
 * @author grp3
 */
public enum Status {
    SUCCESS, FAIL, BID_TO_LOW, AUCTION_ENDED;
}
